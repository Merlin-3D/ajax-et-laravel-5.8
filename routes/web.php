<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|



Route :: ressource ( 'users' , [ 'uses' => 'UserController @ index' , 'as' => 'users.index' Route::get('users','UserController@index')->name('users.index');]);
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('users','UserController@index')->name('users.index');

Route::get('users-delete/destroy/{id}', 'UserController@destroy');

Route::post('ajax-crud/update', 'AjaxCrudController@update')->name('ajax-crud.update');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
