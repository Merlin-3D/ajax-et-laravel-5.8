<!DOCTYPE html>
<html>
<head>
    <title>Laravel 5.8 Datatables Tutorial - ItSolutionStuff.com</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
      <link href="{{ URL::asset('vendor/datatables/dataTables.bootstrap4.css')}}" rel="stylesheet">

      <link href="{{ URL::asset('css/sb-admin.css')}}" rel="stylesheet">

    <script src="{{ URL::asset('vendor/jquery/jquery-3.4.1.js')}} "></script>
    <script src="{{ URL::asset('vendor/datatables/jquery.dataTables.js')}} "></script>

    <script src="{{ URL::asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}} "></script>
    <script src="{{ URL::asset('vendor/datatables/dataTables.bootstrap4.js')}} "></script>

</head>
<body>

<div class="container">
    <h1>Laravel 5.8 Datatables Tutorial <br/> HDTuto.com</h1>
    <table class="table table-bordered data-table">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Email Verifiat</th>

                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<div id="formModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New Record</h4>
        </div>
        <div class="modal-body">
         <span id="form_result"></span>
         <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
          @csrf
          <div class="form-group">
            <label class="control-label col-md-4" >First Name : </label>
            <div class="col-md-8">
             <input type="text" name="first_name" id="first_name" class="form-control" />
            </div>
           </div>
           <div class="form-group">
            <label class="control-label col-md-4">Last Name : </label>
            <div class="col-md-8">
             <input type="text" name="last_name" id="last_name" class="form-control" />
            </div>
           </div>
           <div class="form-group">
            <label class="control-label col-md-4">Select Profile Image : </label>
            <div class="col-md-8">
             <input type="file" name="image" id="image" />
             <span id="store_image"></span>
            </div>
           </div>
           <br />
           <div class="form-group" align="center">
            <input type="hidden" name="action" id="action" />
            <input type="hidden" name="hidden_id" id="hidden_id" />
            <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add" />
           </div>
         </form>
        </div>
     </div>
    </div>
</div>

<div id="confirmModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title">Confirmation</h()>
            </div>
            <div class="modal-body">
                <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
            </div>
            <div class="modal-footer">
             <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>


</body>

<script type="text/javascript">
$(document).ready(function(){
  $(function () {

    $('.data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ route('users.index') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex'},
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
            {data: 'email_verified_at', name: 'email_verified_at'},

            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });

  });
var id;
  $(document).on('click', '.edit', function(){
  id = $(this).attr('id');
  $('#formModal').modal('show');

  });

var user_id;

  $(document).on('click', '.delete', function(){
  user_id = $(this).attr('id');
  $('#confirmModal').modal('show');

  });

  $('#ok_button').click(function(){

  $.ajax({
    url:"users-delete/destroy/"+user_id,
    beforeSend:function(){
    $('#ok_button').text('Deleting...');
    },
    success:function(data)
    {
    setTimeout(function(){
     $('#confirmModal').modal('hide');
    // $('#user_table').DataTable().ajax.reload();
    $('.data-table').DataTable().ajax.reload();

    }, 2000);
    }
  })


  });

  })
</script>
</html>
